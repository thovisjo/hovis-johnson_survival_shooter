﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour
{
    public bool walkable = true;
    public bool current = false;
    public bool target = false;
    public bool selectable = false;

    //a list for the adjacent/neighbor tiles
    public List<TileScript> adjacencyList = new List<TileScript>();


    //needed for Breadth first search
    public bool visited = false;
    public TileScript parent = null;
    public int distance = 0;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //visual cues about where the player can move
        Color usecolor = Color.white;
        if (current)
        {
            usecolor = Color.magenta;
        }
        else if (target)
        {
            usecolor = Color.green;
        }
        else if (selectable)
        {
            usecolor = Color.red;

        }

        usecolor = new Color(usecolor.r, usecolor.g, usecolor.b, GetComponent<Renderer>().material.color.a);
        GetComponent<Renderer>().material.color = usecolor;
        
    }

    public void Reset()
    {
        adjacencyList.Clear();
        walkable = true;
        current = false;
        target = false;
        selectable = false;


        //needed for Breadth first search
        visited = false;
        parent = null;
        distance = 0;
    }
    public void FindNeighbors(float jumpHeight)
    {
        Reset();

        CheckTile(Vector3.forward, jumpHeight);
        CheckTile(-Vector3.forward, jumpHeight);
        CheckTile(Vector3.right, jumpHeight);
        CheckTile(-Vector3.right, jumpHeight);

    }
    public void CheckTile(Vector3 direction,float jumpHeight)
    {
        Vector3 halfExtents = new Vector3(.25f, (1 + jumpHeight)/2, .25f);
        Collider[] colliders = Physics.OverlapBox(transform.position + direction, halfExtents);

        foreach (Collider item in colliders)
        {
            TileScript tile = item.GetComponent<TileScript>();
            if(tile != null && tile.walkable)
            {
                RaycastHit hit;
                if(!Physics.Raycast(tile.transform.position, Vector3.up, out hit, 1))
                {
                    adjacencyList.Add(tile);
                }
                
            }
        }
    }
}   

