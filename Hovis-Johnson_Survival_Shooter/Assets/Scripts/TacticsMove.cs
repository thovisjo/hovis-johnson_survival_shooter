﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacticsMove : MonoBehaviour
{
    // this is the base class for tactical movement
    public bool turn = false;
    List<TileScript> selectableTiles = new List<TileScript>();
    GameObject[] tiles;

    Stack<TileScript> path = new Stack<TileScript>();
    TileScript currentTile;

    public bool moving = false;
    
    public int move = 5;
    public float jumpHeight = 2;
    public float moveSpeed = 5;


    Vector3 velocity = new Vector3();
    Vector3 heading = new Vector3();

    float halfHeight = 0;


    protected void Init()
    {
        tiles = GameObject.FindGameObjectsWithTag("Tile");
        halfHeight = GetComponent<Collider>().bounds.extents.y;

        //TurnManager.AddUnit(this);


    }

    public void GetCurrentTile()
    {
        currentTile = GetTargetTile(gameObject);
        currentTile.current = true;
    }

    public TileScript GetTargetTile(GameObject target)
    {
        RaycastHit hit;
        TileScript tile = null;
        if (Physics.Raycast(target.transform.position, -Vector3.up, out hit, 2))
        {
            tile = hit.collider.GetComponent<TileScript>();
        }

        return tile;
    }

    public void ComputeAdjacencyList()
    {
        foreach(GameObject tile in tiles)
        {
            TileScript t = tile.GetComponent<TileScript>();
            t.FindNeighbors(jumpHeight);
        }
    }

    public void FindSelectableTiles()
    {
        ComputeAdjacencyList();
        GetCurrentTile();

        Queue<TileScript> process = new Queue<TileScript>();

        process.Enqueue(currentTile);
        currentTile.visited = true;

        while (process.Count > 0)
        {
            TileScript t = process.Dequeue();

            selectableTiles.Add(t);
            t.selectable = true;

            if (t.distance < move)
            {
                foreach (TileScript tile in t.adjacencyList)
                {
                    if (!tile.visited)
                    {
                        tile.parent = t;
                        tile.visited = true;
                        tile.distance = 1 + t.distance;
                        process.Enqueue(tile);
                    }
                }
            }
        }
    }
    public void MoveToTile(TileScript tile)
    {
        path.Clear();
        tile.target = true;
        moving = true;

        TileScript next = tile;
        while(next != null)
        {
            path.Push(next);
            next = next.parent;

        }
    }

    public void Move()
    {
        if(path.Count > 0)
        {
            TileScript t = path.Peek();
            Vector3 target = t.transform.position;

            //calculate unit position on top of target tile
            target.y += .1f;

            if(Vector3.Distance(transform.position, target) >= .15f)
            {
                CalculateHeading(target);
                SetHorizontalVelocity();

                transform.forward = heading;
                transform.position += velocity * Time.deltaTime;
            }
            else
            {
                transform.position = target;
                path.Pop();
            }

        }
        else
        {
            RemoveSelectableTiles();
            moving = false;
            TurnManager.turn += 1;
        }

        TurnManager.EndTurn();

    }

    protected void RemoveSelectableTiles()
    {
        if(currentTile != null)
        {
            currentTile.current = false;
            currentTile = null;
        }

        foreach(TileScript tile in selectableTiles)
        {
            tile.Reset();
        }
        selectableTiles.Clear();
    }

    void CalculateHeading(Vector3 target)
    {
        heading = target - transform.position;
        heading.Normalize();
    }
    void SetHorizontalVelocity()
    {
        velocity = heading * moveSpeed;
    }

    public void BeginTurn()
    {
        turn = true;
    }
    public void EndTurn()
    {
        turn = false;
    }
}
